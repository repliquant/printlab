# Mode d'emploi — erg — 2018

# 1 Plotter Roland

Roland DXY1100

- Préparer un dessin en hpgl

Ouvrir les fichier svg avec Inkscape.
Sauvegarder au format hpg en décochant l'option “mirror Y-axis”


- Se connecter au plotter

Ouvrir un terminal
Taper “sudo chmod 777 /dev/ttyUSB0”
Valider avec la touche “Entrée”
Compléter le mot de passe (il reste en blind mode c'est normal)
Valider avec la touche “Entrée”


- Lancer le dessin

Depuis le terminal, déplacer vous vers le dossier de travail avec la commande suivant :
$ cd Documents/hpgl
Entrer la commande suivante pour envoyer le dessin au plotter :
$ plot_hpgl_file.py mondessin.hpgl
Sélection l'option 2 ou 11 et valider avec la touche “Entrée”

# 2 Cameo

- Utiliser l'ordinateur du Printlab
- Utiliser Inkscape

## infos complémentaires sur les plotters

Pour le plotter Roland, il faut souvent vérifier qu'il est branché, et être très patient. Il arrive souvent qu'il y ait des faux contacts. Si vous arrivez à caler la prise, bouger le plotter le moins possible, il est très sensible. Il est aussi lent à démarrer, il a peu de mémoire donc il lui faut son temps pour traiter le fichier qu'on lui envoie.

Pour écrire des textes au plotter, il faut d'une part qu'il soient vectorisés, d'autre part utiliser des polices spéciales (si vous ne voulez pas que le plotter dessine le tous les coutours de la lettre, mais seulement l'"intérieur", le ductus). Il faut utiliser pour cela des **Hersheys fonts**.

Le lien vers la documentation d'Axidraw (une marque de plotters) qui donne pas mal de conseils sur Inkscape et différents plugins qui peuvent servir au plotter
[wiki Axidraw](https://wiki.evilmadscientist.com/AxiDraw#Advanced_topics_and_tips)

# 3 Riso

## l’impression riso
création, développement, utilisation, contexte de création
La *risographe* ou *duplicopieur* est au départ une machine pour reproduire des documents en grande quantité (comme une photocopieuse). L'avantage étant de pouvoir faire des reproductions avec des encres de couleur, au même prix qu'avec de l'encre noire (et à un prix leplus bas possible).

Son ancêtre est le *miméographe*, qui fonctionne sur le même principe de pochoir. Cependant le nombre de tirage est limité, car la feuille utilisée pour le pochoir se dégrade assez rapidement au fur et à mesure que l'encre passe à travers.

Noboru Hayama, un japonais, met au point le premier duplicopieur en 1946, dans l'objectif d'augmenter la quantité de tirages réalisables avec un seul pochoir. Il crée également l'atelier Riso-Sha, qui fabrique et vend ces machines, et Riso-Ink, des encres maison, en 1954. En 1963, l'atelier Riso-Sha devient Riso Kagaku Corporation. En japonais "Riso" signifie "idéal", "kagaku" "sciences" et "graph" "écriture".

Même si au départ les machines sont plus souvent utilisées pour reproduire des docuents textuels, pour des administrations par exemple, le choix de couleurs, le prix abordable et la texture particulière de l'impression attirent les graphistes et les artistes. C'est la solution la plus économique pour faire des reproductions allant de 20 à 10000 exemplaires environ.

Aujourd'hui, Riso est présent dans 150 pays et une communauté grossissante se rassemble autour de cette technique.

## Le tambour (Thomas)
Le *tambour* est la partie qui contient l'encre. Un tambour = une seule encre donc une couleur. Il est très fragile, le moindre coup peut le déformer et donc déformer la surface d'encrage, ce qui créera des erreurs d'impression.

Il faut changer le tambour à chaque fois que l'on fait un master/que l'on change de couleur.

Pour changer le tambour, il faut :
- ouvrir le capot
 ![26](images/tambour_master/changement_de_tambour/26.PNG)

- vérifier que l'indicateur de la touche de désengagement du tambour d'impression soit allumée
Sinon, appuyer sur la touche de désengagement du tambour d'impression
![27](images/tambour_master/changement_de_tambour/27.PNG)

- attraper la poignée du tambour et tirer à fond (mais dourcement) jusqu'à ce qu'il s'arrête
![28](images/tambour_master/changement_de_tambour/28.PNG)

- retirer le tambour en le soulevant (à 2 mains !)
![29](images/tambour_master/changement_de_tambour/29.PNG)

- le ranger dans sa boîte tout de suite (ne pas le laisser traîner pour éviter tout accident) et dans l'étagère

- sortir le nouveau tambour de la boîte (à 2 mains)

- le remettre doucement dans l'emplacement
![31](images/tambour_master/changement_de_tambour/31.PNG)

- le pousser au fond jusqu'à entendre un petit **clac**

- aligner les pointes de flèches en tournant le tambour jusqu'à entendre un petit **clac** (sans mettre ses doigts sur le master !) si le tambour ne se pousse pas au fond
![32](images/tambour_master/changement_de_tambour/32.PNG)

- refermer la porte

Entretien du tambour :
Pour changer la cartouche d'encre, il faut :
- ouvrir le capot
![1](images/tambour_master/remplacement_cartouche_encre/1.PNG)

- retirer la cartouche vide : tourner la cartouche (dans le tambour) dans le sens inverse des aiguilles d'une montre puis tirer dessus
![2](images/tambour_master/remplacement_cartouche_encre/2.PNG)

- retirer le capuchon de la nouvelle cartouche **ne pas toucher la surface de sortie de la nouvelle cartouche**
![3](images/tambour_master/remplacement_cartouche_encre/3.PNG)

- insérer et verrouiller la nouvelle cartouche (dans le sens des aiguilles d'un montre)
![5](images/tambour_master/remplacement_cartouche_encre/5.PNG)
![7](images/tambour_master/remplacement_cartouche_encre/7.PNG)

- refermer le capot
![8](images/tambour_master/remplacement_cartouche_encre/8.PNG)


La cartouche est composée de platique (ploypropylène, polyéthylène). pour la jeter, il faut retirer la petite ériquette en métal (en faisant attention à ne pas se salir) puis les jeter séparément.

L'encre en elle-même est composée d'hydrocarbure de pétrole, d'eau et de pigment.

## Le master
L'image est envoyée à la machine pour être reproduite en trame sur un *master* (matrice en français). Ce master est entouré autour du tambour, chargé d'encre. L'encre va ensuite passer au travers avant de se déposer sur le papier. C'est ni plus ni moins qu'un pochoir. Un master = un pochoir = une couleur. Il faut donc créer autant de master que de passages (un par couleur).
Le master est ce qui coûte le plus cher en riso (3,10€ l'unité). Avec un seul master on peut faire énormément de copies, il vaut donc mieux éviter de faire un master pour seulement 3 pages… PLus on fait de copies plus le master est rentable.

Il se trouve sous forme de rouleau. À chaque nouveau master, le précédent master (qui reste sur le tambour) est enlevé, puis un master vierge est enroulé autour du tambour.

Il se trouve sous forme de rouleau et est composé de papier japon recouvert de plastique. À chaque nouveau master, le précédent master (qui reste sur le tambour) est enlevé, puis un master vierge est enroulé autour du tambour. Puis, des petites têtes chauffantes vont microperforer le master selon la trame choisie, afin de laisser l'encre passer. C'est la création du "pochoir".

Pour remplacer le rouleau de matrice, il faut :
- ouvrir le capot
![10](images/tambour_master/remplacement_rouleau_matrice/10.PNG)

- Vérifier que l'indicateur de la touche de désengagement de l'unité de création de matrice soit allumé.
Sinon, appuyez sur la touche de désengagement de l'unité de création de matrice
![11](images/tambour_master/remplacement_rouleau_matrice/11.PNG)

- retirer l'unité de création de matrice entirant sur la poignée jusqu'à ce qu'elle s'arrête
![12](images/tambour_master/remplacement_rouleau_matrice/12.PNG)

- attraper le levier pour ouvrir le couvercle
![13](images/tambour_master/remplacement_rouleau_matrice/13.PNG)

- ouvrir le support de rouleau de matrice
![14](images/tambour_master/remplacement_rouleau_matrice/14.PNG)

- retirer le rouleau de matrice usagé
![15](images/tambour_master/remplacement_rouleau_matrice/15.PNG)

- installer un nouveau rouleau de matrice. Enlever le film d'emballage et placer le rouleau pour que la marque  soit sur la gauche.
![16](images/tambour_master/remplacement_rouleau_matrice/16.PNG)

- refermer le support de rouleau de matrice et enlever la bande adhésive
![17](images/tambour_master/remplacement_rouleau_matrice/17.PNG)

- insérer le bord d'attaque du rouleau de matrice dans son entrée (sous le rabat du guide de matrice). Rembobiner en serrant la bride de droite vers l'intérieur pour tendre légèrement le master s'il est trop lâche
![18](images/tambour_master/remplacement_rouleau_matrice/18.PNG)
![19](images/tambour_master/remplacement_rouleau_matrice/19.PNG)

L'axe central du rouleau de matrice est en papier, mais la petite partie où la marque  est imprimée est en plastique, métal et papier. Il faut la retirer et la jeter séparément pour faciliter le recyclage.

Pour vider la boîte de récupération des matrices, il faut :
- ouvrir le capot
![20](images/tambour_master/vider_la_récupération_des_matrices/20.PNG)

- tirer vers soi la boîte de récupération des matrices
![21](images/tambour_master/vider_la_récupération_des_matrices/21.PNG)

- jeter les matrices usagées. Si elles collent, appuyer sur le levier situé sur la poignée
![22](images/tambour_master/vider_la_récupération_des_matrices/22.PNG)

- replacer la boîte de récupération des matrices jusqu'à son arrêt
![23](images/tambour_master/vider_la_récupération_des_matrices/23.PNG)

Les matrices sont pleines d'encre, il ne faut donc pas les jeter avec le papier recyclé


Ne pas stocker les consommables dans les endroits suivants:
– Endroits soumis aux rayons directs du soleil ou endroits éclairés situés près de fenêtres.
(S'il n'y a pas le choix, mettre des rideaux à la fenêtre.)
– Les endroit sujets à des changements rapides de température
– Emplacements très chauds et humides ou emplacements très froids et secs

- composition
Le master est en papier à base d’acétate recouvert de cire.

## L'impression

## la machine RZ370
cf guide **utilisateur**
recueillir un maximum d'informations, de scans, pour organiser une présentation simple et efficace
- Schémas machine général + Panneau de contrôle (p.6-11)

![schéma](images/schémas_machine/pièces_et_composants.PNG)

 1) Couvercle d'original
 2) Panneau de contrôle secondair
 3) Vitre d'exposition
Placez un original face vers le bas.
 4) Affichage principal
 5) Panneau de contrôle principal
 6) Boîte de récupération des matrices
Recueille les matrices éjectées.
 7) Levier de réglage de la pression
d'alimentation papier
Règle la pression d'alimentation du papier suivant
le papier utilisé.
 8) Touche d'abaissement du plateau
d'alimentation du papier
Abaisse le plateau d'alimentation du papier lors du
remplacement ou de l'ajout de papier.
 9) Plateau d'alimentation du papier
 10) Guide d'alimentation papier
Encadrent et guident le papier.
Faire glisser pour ajuster au format du papier.
 11) Levier de blocage du guide d'alimentation
du papier
Bloque les guides d'alimentation papier.
 12) Molette de réglage de position
d'impression horizontale
Déplace la position d'impression vers la gauche ou
vers la droite.
 13) Butée d'originaux de l'AAD (Option)
Arrête les originaux numérisés par l'unité AAD.
 14) Réceptacle d'originaux AAD (Option)
 15) Touche de déblocage des originaux AAD (Option)
Appuyez pour désengager les originaux de l'unité
de l'AAD en cas de bourrage du papier ou si les
originaux ont besoin d'être repositionnés.
 16) Guides des originaux de l'AAD (Option)
Maintiennent et guident les originaux dans l'unité AAD.
Faire glisser pour ajuster au format du papier.
 17) Feuille blanche d'AAD (Option)
 18) Molette de déblocage d'originaux AAD (Option)
Permet de désengager les originaux passés dans
l'AAD en cas de bourrage du papier.
 19) Vitre de scanner AAD (Pour RZ3XX, en option)
 20) Couvercle avant
 21) Support de capuchon de cartouche d'encre
 22) Unité de création de matrice
 23) Couvercle de l'unité de création de matrice
 24) Rabat de guide de matrice
 25) Rouleau de matrice
 26) Support de rouleau de matrice
 27) Cartouche d'encre
 28) Poignée du tambour
 29) Tambour
 30) Compteur
Compte le nombre de copies (compteur total de copies)
et le nombre de matrices créées (compteur de matrice).
 31) Touche de désengagement du tambour
Débloque le tambour pour l'enlever.
 32) Poignée d'unité de création de matrice
 33) Touche de déblocage de l'unité de
création de matrice
Débloque l'unité de création de matrice pour l'enlever.
 34) Stabilisateur
 35) Interrupteur d'alimentation
 36) Molette de réglage de l'aile d'éjection du papier
(Pour RZ2XX, non disponible)
Ajuster selon la finition et le format du papier afin
d'aligner les épreuves imprimées.
 37) Dispositif à papier (Pour RZ2XX, non disponible)
Appuyez pour ouvrir le capot et aligner le papier
imprimé.
 38) Guide de réception des copies
Aligne soigneusement les copies imprimées.
Faire glisser selon la largeur du papier à imprimer.
 39) Butée de papier
Elle arrête les copies imprimées et éjectées dans
le Réceptacle du papier.
 40) Réceptacle du papier

![schéma2](images/schémas_machine/panneau_de_contrôle.PNG)

 1) Vérifier Message d'erreur
Indique l'emplacement et le statut de l'erreur.
 2) Indicateur
 3) Affichage de quantité d'impression
(Affichage du numéro d'erreur)
Montre le nombre de copies imprimées, les
valeurs numériques entrées pour les différents
réglages, et les numéros d'erreur.
 4) Touche de création de matrice
Prépare l'imprimante à la création de matrices.
 5) Flèches de progression
Indique l'état d'avancement de la création et
impression de la matrice.
Lorsque la création de matrice a été effectuée,
tous les indicateurs au-dessus de la touche de
création de matrice s'allument.
Lorsque l'impression est prête, tous les indicateurs
au-dessus de la touche d'impression s'allument.
 6) Touche d'impression
Prépare l'imprimante à l'impression.
 7) Touche de réveil
Active l'imprimante lorsqu'elle est en mode d'économie
d'énergie.
 8) Touche de marche automatique/Indicateur
Effectue un fonctionnement sans interruption
depuis la création de matrice jusqu'à l'impression.
Lorsqu'elle est activée, l'indicateur situé à côté de
la touche s'allume.
 9) Touches de réglage de la vitesse
d'impression/Indicateur
Sélectionnez la vitesse d'impression sur cinq niveaux.
L'indicateur situé au dessus des touches montre la
vitesse actuelle.
 10) Touches de réglage de densité d'impression/
Indicateur (Pour RZ2XX, non disponible)
Choisir la densité d'impression entre les cinq
niveaux proposés.
L'indicateur situé au-dessus des touches montre le
niveau de densité actuel.
 11) Touches de réglage de position
d'impression verticale/Indicateur
Règle la position d'impression en direction verticale
(sur ±15 mm) après avoir créé une matrice.
L'indicateur situé au dessus des touches montre la
quantité de décalage par rapport au centre.
Pour effacer la quantité de décalage, appuyer sur .
 12) Touche
Utiliser lors de l'impression à l'aide d'une imprimante
périphérique optionnelle.
 13) Touches de nombre de copies (de 0 à 9)
Permet d'entrer le nombre de copies à imprimer ou
d'autres valeurs numériques.
 14) Touche C
Annule les valeurs numériques entrées ou remet le
compteur à zéro.
 15) Touche P/Indicateur
Permet à l'imprimante d'imprimer et de regrouper les
copies suivant spécifications (impression programmée).
Lorsqu'elle est activée, l'indicateur situé au-dessus
de la touche s'allume.
 16) +Touche
Utiliser lors du réglage d'une impression programmée
ou lors du changement des réglages initiaux.
 17) × Touche
Utiliser lors du réglage d'une impression programmée.
 18) Touche départ
Commence la création de matrice ou la procédure
d'impression ou exécute les opérations spécifiées.
La touche est allumée uniquement si la touche est active.
 19) Touche d'essai
Utiliser lorsque vous voulez vérifier le résultat de
l'impression après avoir ajusté, par exemple, la
position d'impression.
Ceci vous permet d'imprimer un échantillon sans
affecter la valeur sur l'affichage du nombre de copies.
 20) Touche de réarmement
Remet tous les réglages tels qu'ils étaient au départ.
 21) Touche d'arrêt
Arrête l'opération en cours.

- Tableau des codes erreur de la machine (guide de dépannage, p.82 du guide, rajout des schémas référencés )
![schéma3](images/schémas_machine/schéma_erreur.PNG)
![schéma4](images/schémas_machine/indicateurs_erreurs.PNG)

## La préparation des fichiers
**Captures écran** photoshop/in design/illustrator
- format de fichier: _pdf N&B compatible avec acrobat 4_
- Décocher les possibilités d'édition et pas de transparences

![acrobat4](images/captures_photoshop/8enregistrer_pdf_acrobat4.jpg)

En enlevant les calques, les transparences, les possibilités d'édition, on évite les mauvaises surprises à l'impression (une image en png qui apparaît transparente à l'écran mais qui laisse un cadre blanc sur papier…)

- résolution maximum

La riso n'a pas de définition plus grande que 600 dpi (pas la peine de faire des fichiers plus lourds). De toute façon, la trame de la riso va forcément réduire les détails les plus fins, même avec la plus grande linéature.

- marges

Les marges de la riso font que la surface d'impression sur un A3 est de 289 x 409 mm. (Marge de pied de 2 mm, marge de tête de 5 mm -le côté qui rentre dans la machine- et 3 mm pour les autres marges). Il faut aussi savoir que la roulette qui prend le papier peut se salir et donc tacher les feuilles suivantes? Pour éviter ça on essaye de ne pas mettre trop d'encre à cet endroit de la page (si possible, on décale  tout vers le bas de la page).

- couleurs/masters

Chaque couleur est un master différent, donc un fichier différent (en niveau de gris ou en noir et blanc). Pour l'ordre, comme en sérigraphie, on imprime les couleurs les plus claires d'abord.

- l'imposition

Pour faire une édition, selon la reliure choisie, il peut être nécessaire de devoir imposer soi-même les pages de l'édition. L'**imposition** est le fait d'arranger les pages sur le format d'impression (ici la page A3) pour optimiser les temps d'impression (imprimer le plus de pages possible sur un seul format), faciliter la reliure (en mettant au bon endroit les pages en vis-à-vis et les recto-verso pour faire des livrets par exemple) et en évitant le gaspillage de papier. Il faut se rappeler que les calages sont hasardeux, donc le recto-verso pas très précis.



## Choisir son papier

- Le sens de coulée du papier

L’orientation des fibres lors de la phase de production du papier détermine ce que l’on appelle le sens des fibres et parfois le sens machine par opposition au sens travers. Le papier est plus rigide et se plie moins facilement le long de cet axe. Cela affecte les sensations liées au toucher du support imprimé. Par exemple, une brochure imprimée avec un sens du papier incorrect sera moins stable, et, s’il s’agit d’un livre, les pages sembleront plus rigides et plus difficiles à tourner.

Il est facile de connaitre le sens machine en posant une feuille de papier sur le bord de la table. Le papier est plus rigide et se plie plus difficilement lorsque les fibres sont perpendiculaire au bord. Une autre astuce consiste à serrer avec les ongles du pouce et de l’index en glissant le long du bord de la feuille. Le bord qui se déforme le plus correspond au sens travers. Les dimensions fournies par le marchant de papier indiquent aussi le sens des fibres, le chiffre donné en premier (par ex, 210 dans le format 210 x 297) indique le côté travers des fibre.

En risographie, le grammage du papier **doit** être entre 60 et 200 g/m2.

- Les papiers à privilégier

Les papiers à privilégier en risographie sont ceux en fibres naturelles. Les papiers écologiques sont les rois de la riso car ce sont des papiers non traités, ce qui permet à l’encre une meilleure adhésion. Il faut impérativement utiliser des papiers non couchés qui peuvent boire l’encre et au contraire ne surtout pas prendre un papier couché qui lui a pour particularité d’être recouvert d’une couche d’enduit ce qui empêche les encres riso d’adhérer. (Exemples de papiers couchés = le Arco Print de Fedrigoni qu’on peut trouver à Paper Factory, rue des Tanneurs à Bruxelles ou encore le Munken Print de Artoc Paper,…)  
Le temps de séchage en riso est indispensable car l’encre a tendance à faire des traces facilement.

- les papiers à ne pas utiliser

Les papiers à ne pas utiliser sont généralement les papiers Gloss, le calque, et tous ceux qui sont à fibres plastiques. Les encres ne peuvent pas sécher sur un papier couché car il est trop lisse. Cela risque de créer des bavures sur le visuel.

## L'impression

Avec la riso de l'école, on peut jouer sur différents paramètres:
-  la vitesse d'impression

La vitesse d'impression peut avoir un effet sur le décalage feuille par feuille du papier, surtout avec du papier fin. Il vaut mieux être un peu plus patient pour ne pas se retrouver avec ses derniers tirages complètements décalés.

- le contraste

On peut augmenter ou diminuer le "contraste" en augmentant ou en diminuant la pression effectuée par le rouleau sur la feuille, ce qui aura pour effet de l'encrer plus ou moins. Vu que la machine est vieille, il est souvent nécessaire d'augmenter un peu voir beaucoup de le contraste, surtout en fin de cartouche.

![paramètres](images/captures_impression/Capture1.PNG)
![choix trames](images/captures_impression/Capture2.PNG)

**Mode granulaire**
travail de superpositions de couleurs

**Mode trame**
Superposer des couches tramées issues d'une séparation CMJN

**C** 15° **M** 75° **J** 0° **N** 45°
Les couches C, M & N sont séparées de 30°

![trame1](images/trame1.png)
![trame2](images/trame2.png)

Usages conventionnels
Cette séparation d'une image en 4 couches de couleurs tramées permet une impression en **quadrichromie** (4 couleurs). Avec du Cyan, du Magenta, du Jaune et Noir, on peut reconstituer par mélange optique presque toutes les couleurs du spectre. La risographie ne permet pas vraiment cela, mais permet au contraire l'utilisation de couleurs pures (et pas des mélanges).

Autres combinaisons possibles
Cependant, on peut utiliser des couleurs proches du CMJN (rose fluo pour magenta, bleu pour cyan etc.) pour faire une "fausse quadrichromie".

Vocabulaire
- la rosace ou œil de perdrix

Lorsque l'on imprime en **quadrichromie** (4 couleurs) avec une trame linéaire, on choisit un angle de trame différent pour chaque couleur, afin que les points ne se superposent pas. Par mélange optique, comme dans le pointillisme, l'oeil reconstitue les couleurs. La superposition des 4 trames (Cyan, Magenta, Jaune et Noir) selon un angle précis donne un "motif" circulaire que l'on appelle **rosace** ou **oeil de perdrix**. Ce motif est inévitable, et caractéritique de la quadrichromie. Les angles sont calculés de façon à rendre ce motif le moins dérangeant possible.

- le moirage

De la même façon qu'en quadrichromie la superposition des trames crée des rosaces, lorsque la différence d'angle entre 2 trames superposées est inférieure à 15° on crée des **moirages**. L'image n'est plus lisible car des lignes apparaissent. Pour éviter ça, si vous superposez des trames linéaires, il faut laisser 30° d'inclinaison de différence entre deux couches superposées.

![moirage](images/moirage.gif)

Application concrète sur une photo choisie :
- 1 ![aplatir](images/captures_photoshop/1aplatir_image.jpg)
- 2 ![cmjn](images/captures_photoshop/2mode_CMJN.jpg)
- 3 ![couches](images/captures_photoshop/3séparer_couches_CMJN.jpg)
- 4 ![niv_gris](images/captures_photoshop/4mode_niv_gris.jpg)
- 5 ![bitmap](images/captures_photoshop/5mode_bitmap.jpg)
- 6 ![trame](images/captures_photoshop/6bitmap_trame.jpg)
- 7 ![demi_teinte](images/captures_photoshop/7trames_demi_teintes.jpg)
- 8 ![acrobat4](images/captures_photoshop/8enregistrer_pdf_acrobat4.jpg)

Déclinaisons sur photoshop avec déclinaisons de motifs :
trame en carrés : ![cercle](images/exemples_trames/carré.jpg)
trame en cercles :![cercle](images/exemples_trames/cercle.jpg)
trame en croix : ![croix](images/exemples_trames/croix.jpg)
trame en losanges : ![losange](images/exemples_trames/losange.jpg)
trame en ellipses : ![ellipse](images/exemples_trames/ellipse.jpg)
trame en droites : ![droite](images/exemples_trames/droite.jpg)

Outils à disposition pour les séparations de couches et le tramage :

- Stochaster http://www.stochaster.org/
- Texturing http://ivan-murit.fr/works.php?w=texturing&p=texturing-fr.htm

Utilisation des motifs de Turing pour différents types de trames.

- Color Library https://colorlibrary.ch/

Des profils colorimétriques personnalisés crées par l'ECAL, en Suisse. Permet des séparations de couches très précises en 2, 3, 4 voire 5 ou 6 couleurs. Au Print Lab sont disponibles 2 color libraries : *rouge/noir* et *bronze/bleu moyen*
![édition](images/color_library/édition.jpg)
![convertir_profil](images/color_library/convertir_en_profil.jpg)
image originale :
![img-cmjn](images/color_library/img-cmjn.jpg)

![rouge-noir](images/color_library/rouge-noir.jpg)
![bleu-bronze](images/color_library/bleu-bronze.jpg)

- MR2018 - Kyuha (Q) Shim | 심규하 | 沈揆夏 [www.kyuhashim.com](http://www.kyuhashim.com/)
 https://github.com/qshim/MR2018/tree/master

 - wiki riso http://stencil.wiki/ beaucoup de documentation, d'information…



## Calages des impressions

# RÉGLAGE DE LA POSITION D’IMPRESSION, CALAGE DES FEUILLES

> **à savoir :** Avec l’imprimante RISO, il faut savoir que les calages restent souvent hasardeux donc imparfait.


## RÉGLAGE VERTICAL

Pour ajuster la position d’impression verticalement;
utilisez les touches de réglage de position d’impression verticale.
 </>

- Touche < : déplacement vers le bas d’environ 0,5 mm.
- Touche > : déplacement vers le haut d’environ 0,5mm.

- Touche -0-: remettre le papier d’impression à sa position initiale.

![image 1](images/riso-RZ-370.jpg)


## RÉGLAGE HORIZONTAL

Appuyez sur la touche d’abaissement du plateau d’alimentation du papier pour le rabaisser et le caler.

Pour ajuster la position d’impression horizontalement;
utilisez la molette de réglage de position d’impression horizontale.
- Tourner la molette vers le haut : déplace la position vers la gauche
- Tourner la molette vers le bas   : déplace vers la droite

La graduation fléchée située à côté du plateau d'alimentation du papier permet de vérifier l'écart vis à vis du centre.
>La flèche de graduation indique le centre.


**Conseil :** Après avoir réglé la position d'impression (ainsi que la densité), il est bien d’effectuer des copies d'essais pour vérifier la nouvelle position d'impression et la qualité d’impression.

## Les problèmes liés au fichier

- Le fichier ne se lance pas

Vérifiez que le fichier a été envoyé à la bonne imprimante (RISO RZ) et pas RICOH.

Si le fichier est assez lourd, il faut être patient car la machine peut mettre du temps à le traiter pour effectuer le master. En cas de doute, vérifier dans l'historique des impressions si le fichier est toujours en train d'être envoyé à la riso. Si vous relancez une impression depuis l'ordi vous risquez de créer 2 masters, donc d'en gaspiller un (et donc de le payer aussi).

- Des différences d'opacités sont constatées sur les images

Vérifier le seuil des images sur Photoshop

- La typo ne s'imprime pas/pas bien

Il faut peut-être la vectoriser avant (cmd+maj+o sur mac et ctrl+maj+o sur windows pour InDesign et Illustrator).

## Les couleurs disponibles au print lab

|                |Équivalents CMJN               |Équivalents Pantone          |Couleurs                        |
|----------------|-------------------------------|-----------------------------|--------------------------------|
|Jaune			 |`O-9-100-0`                    |Yellow U                     |![image 1](images/j.jpg)        |
|Rose fluo       |`0-72-31-0`                    |485 U                        |![image 2](images/ro.jpg)       |
|Orange fluo     |`0-55-53-0`                    |806 U                        |![image 3](images/o.jpg)        |
|Rouge			 |`0-60-63-0`                    |Warm Red U                   |![image 4](images/r.jpg)        |
|Vert            |`73-0-81-0`                    |354 U                        |![image 5](images/v.jpg)        |
|Bleu fédéral    |`100-65-1-17`                  |288 U                        |![image 6](images/b.jpg)        |
|Noir            |`0-0-0-100`                    |Black U                      |![image 7](images/n.jpg)        |
